package com.example.fragment_vibelabprj

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragment_vibelabprj.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    lateinit var bindingClass: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentHomeBinding.inflate(inflater)

        bindingClass.bNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.goToLogin -> {
                    Navigation.findNavController(bindingClass.root)
                    .navigate(R.id.navigateToLoginFragment)
                }
                R.id.goToWebFragment -> {Navigation.findNavController(bindingClass.root)
                    .navigate(R.id.navigateToWebFragment)}
            }
            true
        }

        return bindingClass.root
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment()
    }
}
