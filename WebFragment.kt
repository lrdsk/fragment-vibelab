package com.example.fragment_vibelabprj

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragment_vibelabprj.databinding.FragmentWebBinding


class WebFragment : Fragment() {
    lateinit var bindingClass: FragmentWebBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentWebBinding.inflate(inflater)
        return bindingClass.root
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = WebFragment()
    }
}
