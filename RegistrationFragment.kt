package com.example.fragment_vibelabprj

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragment_vibelabprj.databinding.FragmentLoginBinding
import com.example.fragment_vibelabprj.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment() {
    lateinit var bindingClass: FragmentRegistrationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentRegistrationBinding.inflate(inflater)

        bindingClass.buttonGoToLogin.setOnClickListener {
            Navigation.findNavController(bindingClass.root)
                .navigate(R.id.navigateToLoginFragment)
        }

        return bindingClass.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}
