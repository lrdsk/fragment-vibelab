package com.example.fragment_vibelabprj

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragment_vibelabprj.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    lateinit var bindingClass: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentLoginBinding.inflate(inflater)

        bindingClass.tvPersonWithOutAccount.setOnClickListener {
            Navigation.findNavController(bindingClass.root)
                .navigate(R.id.navigateToRegisterFragment)
        }

        bindingClass.buttonGoToHomeFragment.setOnClickListener {
            Navigation.findNavController(bindingClass.root)
                .navigate(R.id.navigateToHomeFragment)
        }
        return bindingClass.root

    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}
